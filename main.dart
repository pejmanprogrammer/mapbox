

import "package:flutter/material.dart";
import 'package:location/location.dart';
import 'package:testlearn/getlocation.dart';
import 'package:testlearn/line.dart';
import 'package:testlearn/mapui.dart';
import 'package:testlearn/page.dart';
import 'package:testlearn/placecircle.dart';
import 'package:testlearn/placesymbolpage.dart';
import 'package:testlearn/scrollingmap.dart';

import 'animationcamera.dart';
import 'fullmappage.dart';
import 'movecamerapage.dart';

main() {
  runApp(MaterialApp(home: MapsDemo()));
}


final List<Page> _allPages = <Page>[

  FullMapPage(),
  MoveCameraPage(),
  PlaceCirclePage(),
  PlaceSymbolPage(),
  ScrollingMapPage(),
  AnimateCameraPage(),
  LinePage(),
  MapUiPage(),
  LocationMap(),

];



class MapsDemo extends StatelessWidget {
  void _pushPage(BuildContext context, Page page) async {
    final location = Location();
    final hasPermissions = await location.hasPermission();
    if (hasPermissions != PermissionStatus.GRANTED) {
      await location.requestPermission();
    }

    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (_) => Scaffold(
          appBar: AppBar(title: Text(page.title)),
          body: page,
        )));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('MapboxMaps examples')),
      body: ListView.builder(
        itemCount: _allPages.length,
        itemBuilder: (_, int index) => ListTile(
          leading: _allPages[index].leading,
          title: Text(_allPages[index].title),
          onTap: () => _pushPage(context, _allPages[index]),
        ),
      ),
    );
  }



}


